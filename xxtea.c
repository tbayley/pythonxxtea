/*!***************************************************************************
 * @file     xxtea.c
 * @brief    XXTEA encryption and decryption functions
 *
 * This file implements XXTEA encryption and decryption functions based on the
 * source code from Wikipedia:
 *
 *  https://en.wikipedia.org/wiki/XXTEA
 *  
 * The Wikipedia source is an improved version of the source code from Needham
 * and Wheeler's original implementation, described in the following reference:
 *
 *  David J. Wheeler and Roger M. Needham (October 1998).
 *  "Correction to XTEA".
 *  Computer Laboratory, Cambridge University, England.
 *  https://www.cix.co.uk/~klockstone/xxtea.pdf
 *
 * To build and run with GCC (-g -O0 are for debug only):
 *
 *    gcc -Wall -g -O0 xxtea.c -o xxtea
 *    ./xxtea
 *
 * Copyright(c) Antony Bayley 2017. All rights reserved.
 *
 * Distributed under the Boost Software License, Version 1.0.
 * See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.TXT
 *
 * Created on: 5 August 2017
 *
 *****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define DELTA 0x9e3779b9
#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z)))

#define KEY_LEN         16
#define INPUT_LEN       44                              // number of characters
#define BLOCK_COUNT     INPUT_LEN / sizeof( uint32_t )  // number of 4-byte blocks

/* function declarations */
void btea(uint32_t *v, int n, uint32_t const key[4]);
void print_as_bytes(const char* prefix, void* pData, size_t len);
int main(void);

/* static variables */
static const uint8_t key[KEY_LEN] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
static const char input[INPUT_LEN] = "The quick brown fox jumps over the lazy dog";  // 43 characters + 1 byte padding


/* function definitions */

/**
 * @brief XXTEA in-place encryption/decryption function.
 * 
 * @param[in,out] v   Pointer to the buffer containing input/output data.
 * @param n           Length of data, in 32-bit words
 * @param[in] key     Pointer to XXTEA key.
 */
void btea(uint32_t *v, int n, uint32_t const key[4]) 
{
    uint32_t y, z, sum;
    unsigned p, rounds, e;
    if (n > 1) 
    {
        /* Coding Part */
        rounds = 6 + 52/n;
        sum = 0;
        z = v[n-1];
        do
        {
            sum += DELTA;
            e = (sum >> 2) & 3;
            for (p=0; p<n-1; p++)
            {
                y = v[p+1]; 
                z = v[p] += MX;
            }
            y = v[0];
            z = v[n-1] += MX;
        } while (--rounds);
    } else if (n < -1)
    {
        /* Decoding Part */
        n = -n;
        rounds = 6 + 52/n;
        sum = rounds*DELTA;
        y = v[0];
        do
        {
            e = (sum >> 2) & 3;
            for (p=n-1; p>0; p--) 
            {
                z = v[p-1];
                y = v[p] -= MX;
            }
            z = v[n-1];
            y = v[0] -= MX;
            sum -= DELTA;
        } while (--rounds);
    }
}


/**
 * @brief Print data as sequence of hexadecimal byte values.
 * 
 * @param prefix String to be printed as a prefix to the hexadecimal byte values.
 * @param pData  Pointer to data, of any type.
 * @param len    Lenght of data, in bytes.
 */
void print_as_bytes(const char* prefix, void* pData, size_t len)
{
    uint8_t* bytes = pData;
    printf("%s", prefix);
    for( size_t i = 0U; i < len; i++)
    {
        printf("%02x ", bytes[i]);
    }
    printf("\n");
}


int main(void)
{
    int n = BLOCK_COUNT;
    uint32_t v[BLOCK_COUNT] = {0,};
    memcpy(v, input, strlen(input));
    printf("Input data = %s\n", (char*) v);
    
    // encrypt
    btea( v, n, (uint32_t*) key );
    print_as_bytes("Encrypted data bytes = ", v, sizeof(v));
    
    // decrypt
    btea( v, -n, (uint32_t*) key );
    printf("Decrypted data = %s\n", (char*) v);
}
