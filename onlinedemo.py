''' XXTEA encryption and decryption demonstration


DESCRIPTION
===========
Demonstrate XXTEA encryption and decryption interworking between PythonXXTEA
class and the online XXTEA encoder and decoder web applications listed below.
Encrypted text is displayed using Base64 encoding, so that it can be cut and
pasted between the Windows or Bash terminal session and the web apps.

    https://www.tools4noobs.com/online_tools/xxtea_encrypt/
    
    https://www.tools4noobs.com/online_tools/xxtea_decrypt/
    
Encrypted text is printed in Base64 encoding, so that it can be cut and pasted
into the XXTEA encryption and decryption web apps listed above.


COPYRIGHT
=========
Copyright(c) Antony Bayley 2017. All rights reserved.
Distributed under the Boost Software License, Version 1.0.
See accompanying file COPYING.BOOST-1_0 or copy at
http://www.boost.org/LICENSE_1_0.TXT

Filename: onlinedemo.py
Created on: 6 August 2017

'''

import base64
from pythonxxtea import PythonXXTEA

# ensure consistent performance of input function for Python 2 and 3
try:
    input = raw_input   # Python2
except:
    pass                # Python3 

key = bytearray('0123456789abcdef')
xxtea = PythonXXTEA(key)

# encryption test
txt = input('\nType some plain text: ')
i = bytearray(txt)
(i, padding_len) = xxtea.pad(i)
e = xxtea.encrypt(i)
e_b64 = base64.b64encode(e)
print('Encrypted data = {}\n'.format(e_b64))
print('''Copy the encrypted text and paste it into the XXTEA decryption web app:
https://www.tools4noobs.com/online_tools/xxtea_decrypt/
using the key "0123456789abcdef"\n''')

# decryption test
print('''When you are done, encrypt a message using the XXTEA encryption web app:
https://www.tools4noobs.com/online_tools/xxtea_encrypt/
using the key "0123456789abcdef". Then enter it below.''')
e_b64 = input('\nType or paste the encrypted text: ')
e = base64.b64decode(e_b64)
d = xxtea.decrypt(e)
print('Encrypted data = {}'.format(d))
