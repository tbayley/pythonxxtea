"""
XXTEA encryption and decryption.


DESCRIPTION
============
The PythonXXTEA class is a pure Python implementation of XXTEA encryption and
decryption algorithms, based on the source code from Wikipedia:

    https://en.wikipedia.org/wiki/XXTEA
    
This is an improved version of the source code from Needham and Wheeler's
original implementation, described in the following reference:

    David J. Wheeler and Roger M. Needham (October 1998).
    "Correction to XTEA".
    Computer Laboratory, Cambridge University, England.
    https://www.cix.co.uk/~klockstone/xxtea.pdf

The original C-code algorithm relies on 32-bit unsigned integer arithmetic.
Care has been taken to emulate 32-bit unsigned arithmetic in Python code by
ANDing any results that may exceed the range 0 .. 4294967295 with the
bit-mask 0xFFFFFFFF.


COPYRIGHT
=========
Copyright(c) Antony Bayley 2017. All rights reserved.
Distributed under the Boost Software License, Version 1.0.
See accompanying file COPYING.BOOST-1_0 or copy at
http://www.boost.org/LICENSE_1_0.TXT

Filename: pythonxxtea.py
Created on: 5 August 2017
"""

# Imports
import struct
from copy import copy


#############################################################################
# PythonXXTEAException class definition
#############################################################################

class PythonXXTEAException(Exception):
    """XXTEA Exception subclass."""


#############################################################################
# PythonXXTEA class definition
#############################################################################

class PythonXXTEA:
    """
    XXTEA encryption and decryption.
    
    The PythonXXTEA class is a pure Python implementation of XXTEA encryption and
    decryption algorithms, based on the C source code from Wikipedia:
    
        https://en.wikipedia.org/wiki/XXTEA
        
    This is an improved version of the source code from Needham and Wheeler's
    original implementation, described in the following reference:
    
        David J. Wheeler and Roger M. Needham (October 1998).
        "Correction to XTEA".
        Computer Laboratory, Cambridge University, England.
        https://www.cix.co.uk/~klockstone/xxtea.pdf

    The XXTEA block cypher can be used to encrypt or decrypt a single fixed-length 
    block of bits. The number of bytes in the block must be an exact multiple of 
    4 bytes, and must also be at least 8 bytes.
    """

    # class constants
    DELTA = 0x9E3779B9
    KEY_LEN = 16        # key length in bytes
    CHUNK_LEN = 4       # encryption / decryption chunk size in bytes


    #####  PUBLIC METHODS  #####

    @classmethod
    def pad(cls, data, padding=0x00):
        """
        Append padding bytes to the data, if necessary.

        Add padding bytes to the end of the data, if necessary to satisy the
        following requirements for XXTEA encoding:
        - The number of bytes must be an exact multiple of 4.
        - The number of data bytes must be at least 8.
          
        :param data:        bytearray data.
        :param padding:     The byte value to use as padding.
        
        :return: (data, n)  Where: data = padded data.
                                      n = number of bytes of padding added.
        """
        pdata = copy(data)  # create a copy to avoid changing data
        plen = (- len(pdata)) % cls.CHUNK_LEN  # exact multiple of 4 bytes
        if len(pdata) + plen < 8:
            plen += 4  # minimum length of 8 bytes
        pdata += bytearray([padding]) * plen
        return (pdata,plen)


    def __init__(self, key, byteorder='<'):
        """
        PythonXXTEA class initializer.

        :param  key:      Encryption key: a Python bytearray of length 16 bytes.
        
        :param byteorder: Set byte-order, using one of the Python "struct"
                          package's byte-order formatting characters:
                            '<' : little-endian
                            '>' : big-endian
                            '@' : native endianness for the host system.
        """
        # configure little-endian or big-endian byteorder
        if byteorder in ('@', '=', '<', '>', '!'):
            self._byteorder = byteorder
        else:
            raise PythonXXTEAException('Byte order must be one of: "@", "=", "<", ">", "!".')

        # convert key to a tuple containing four uint32_t values
        if len(key) != self.KEY_LEN:
            raise PythonXXTEAException('Key length must be 16 bytes.')
        self._key = struct.unpack(self._byteorder + 'IIII', key)


    def encrypt(self, data):
        """
        XXTEA encrypt n blocks of length 4 bytes, where n > 1.
        
        Input and output data are of type bytearray.  The encryption block size
        is 4 bytes, due to the XXTEA algorithm's reliance on 32-bit integer
        arithmetic.
        
        :param data: bytearray data of length (n * 4) bytes, where n > 1.
        
        :return:     Encrypted bytearray data of the same length as input.
        """
        if len(data) % self.CHUNK_LEN != 0:
            raise PythonXXTEAException(f'Data length must be a multiple of {self.CHUNK_LEN} bytes')

        # number of blocks
        n = len(data) // self.CHUNK_LEN
        if n <= 1:
            raise PythonXXTEAException('Data length must be at least 8 bytes.')

        # convert input data to uint32_t values
        fmt = 'I' * n
        v = list(struct.unpack(self._byteorder + fmt, data))

        # encrypt
        rounds_init = 6 + (52 // n)
        acc = 0
        z = v[n - 1]
        for rounds in range(rounds_init, 0, -1):
            acc = (acc + self.DELTA) & 0xFFFFFFFF
            e = (acc >> 2) & 3
            for p in range(n - 1):
                y = v[p + 1]
                v[p] = (v[p] + self._mx(y, z, acc, p, e)) & 0xFFFFFFFF
                z = v[p]
            p += 1  # increment loop counter on exit, to emulate C for loop
            y = v[0]
            v[n - 1] = (v[n - 1] + self._mx(y, z, acc, p, e)) & 0xFFFFFFFF
            z = v[n - 1]
        # convert output data to bytearray values
        return bytearray(struct.pack(self._byteorder + fmt, *v))


    def decrypt(self, data):
        """
        XXTEA decrypt n blocks of length 4 bytes, where n > 1.
        
        Input and output data are of type bytearray.  The decryption block size
        is 4 bytes, due to the XXTEA algorithm's reliance on 32-bit integer
        arithmetic.
        
        :param data: bytearray data of length (n * 4) bytes, where n > 1.
        
        :return:     Decrypted bytearray data of the same length as input.
        """
        if len(data) % self.CHUNK_LEN != 0:
            raise PythonXXTEAException(f'Data length must be a multiple of {self.CHUNK_LEN} bytes')

        # number of blocks
        n = len(data) // self.CHUNK_LEN
        if n <= 1:
            raise PythonXXTEAException('Data length must be at least 8 bytes.')

        # convert input data to uint32_t values
        fmt = 'I' * n
        v = list(struct.unpack(self._byteorder + fmt, data))

        # decrypt
        rounds_init = 6 + (52 // n)
        acc = (rounds_init * self.DELTA) & 0xFFFFFFFF
        y = v[0]
        for rounds in range(rounds_init, 0, -1):
            e = (acc >> 2) & 3
            for p in range(n - 1, 0, -1):
                z = v[p - 1]
                v[p] = (v[p] - self._mx(y, z, acc, p, e)) & 0xFFFFFFFF
                y = v[p]
            p -= 1  # decrement loop counter on exit, to emulate C for loop
            z = v[n - 1]
            v[0] = (v[0] - self._mx(y, z, acc, p, e)) & 0xFFFFFFFF
            y = v[0]
            acc = (acc - self.DELTA) & 0xFFFFFFFF
        # convert output data to bytearray values
        return bytearray(struct.pack(self._byteorder + fmt, *v))


    #####  PRIVATE METHODS  #####

    def _mx(self, y, z, acc, p, e):
        """
        Private helper function for XXTEA encryption and decryption.
        """
        return (
            ((z >> 5 ^ ((y << 2) & 0xFFFFFFFF)) + (y >> 3 ^ ((z << 4) & 0xFFFFFFFF))) ^
            ((acc ^ y) + (self._key[(p & 3) ^ e] ^ z))
        ) & 0xFFFFFFFF


if __name__ == '__main__':

    from binascii import hexlify

    # If module is run as a script, demonstrate XXTEA encoding and decoding
    key = bytearray((0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF))
    xxtea = PythonXXTEA(key)

    input_data = bytearray('The quick brown fox jumps over the lazy dog', 'ascii')   # 43 bytes
    (padded_data, padding_len) = xxtea.pad(input_data)
    print(f'Padding length = {padding_len}')
    print(f'Padded input data = {padded_data}')

    enc_data = xxtea.encrypt(padded_data)
    print(f'Encrypted data bytes = {hexlify(enc_data, " ", 1)}')

    dec_data = xxtea.decrypt(enc_data)
    print(f'Decrypted data = {dec_data}')
